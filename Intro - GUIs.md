# Introduction aux Interfaces Graphiques en Python

Auteur : **Sascha Sallès**, Ingésup

Pour créer des interfaces graphiques en Python un vaste choix de librairies existent. Les plus connues et utilisées sont **Tkinter**, **Pygame**, **PyQt** et **PyGTK**

## Prérequis :  
Avoir `python3` d'installé ainsi que `pip`    
Si vous avez installé Python3 correctement à partir du site [Python officiel](https://www.python.org/downloads/), `pip` est déjà préalablement inclus. 
  
Sinon vous faite un petit `curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py`  
Puis `python get-pip.py`.


## Tkinter
> Tkinter est une librairie d'interface graphique integrée directement à python.
Elle est très simple à prendre en main.

* Avantages : Tkinter peux subvenir à la majorité de nos besoin, et est simple à prendre en main.
* Inconvénients : Produit des GUIs type années 90 (ne pas s'attarder sur l'aspect visuel).

### Installation et liens utiles

**Tkinter** est normalement installé de base dans python, mais bon au cas où :  
Pour installer Tkinter sur linux :  
* Ubuntu/Debian : `sudo apt-get install python3-tk`

Lien vers une initiation très intuitive complémentaire à la mienne de tkinter. 
* [Initiation Tkinter](http://pythonlandais.fr/cours_python/cours_python_ch8.php)
* [Initiation Chap2](http://pythonlandais.fr/cours_python/cours_python_ch10.php)

Ce sont de très bon liens avec des exemples concis et de nombreux td/tp(s) afin de vous exercer.

Mes remerciements à M.Smadja pour sa documentation, son site ainsi que le temps passé à m'expliquer les concepts clés de Python. 

### Utilisation 
Lien vers le [Cours dédié](./cours_dediés/tkinter.md)

## Pygame

> Pygame est une bibliothèque libre et open source développée par Pete Shinners. 
Elle est construite à partir de la librairie [SDL](https://en.wikipedia.org/wiki/Simple_DirectMedia_Layer) une librairie écrite en C assez ancienne, mais ultra efficace pour faire des env 2D).
 
Assez facile à prendre en main, ses grandes qualités sont : la gestion de l'audio, la gestion des animés et de la video. 
Vous pouvez aussi créer tout un tas de formes avec pygame.  
Autrement dit : 
Vous allez enfin pouvoir coder pokemon 😎! (version cristal.. calmez vous) 

> Petite mise au point, si vous souhaitez faire des jeux vidéos ultra poussés, n'ésperez pas les faires avec pygame. Pygame est certe ultra performant, et il est tout à fait possible de faire des bons petits jeux 2D, mais ça s'arrete là. Pour des jeux 3d poussés
je déconseille l'utilisation de python et vous conseille plutôt d'utiliser des languages tels que C++ ou java.

### Installation

Avec `pip`:  
* `pip3 install pygame`  
où alors : 
* `python3 -m pip install -U pygame --user`   
Pour vérifier si tout roule, vous pouvez tester grâce au fichier de test intégré en lançant : 
* `python3 -m pygame.examples.aliens`

### Utilisation

* Le [cours dédié](./cours_dediés/pygame.md)
* La [documentation](https://www.pygame.org/docs/) (je vous l'accorde la doc est immonde)

## PyQt 

> PyQt est un module libre python qui reprend les concepts de Qt, la bibliothèque graphique de C++. 

Cette fameuse bibliothèque est cross platform, et est ultra utilisée dans la conception d'interface graphiques.  
Le lien vers la [doc](https://www.qt.io) (celle là est carrément useful)  
Pour info Qt, est développé pour deux utilisations : 
* Une utilisation open source, gratuite (celle que l'on utilisera), ci joint le lien pour telecharger la version [gratos](https://www.qt.io/download-qt-installer?hsCtaTracking=99d9dd4f-5681-48d2-b096-470725510d34%7C074ddad0-fdef-4e53-8aa8-5e8a876d6ab4)
* Une utilisation payante. 

 
