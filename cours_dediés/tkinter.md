# Tkinter


**Rappel**
> Si vous n'êtes pas familier avec l'orienté objet, cela risque d'être compliqué de manière générale avec les GUI...  
Je n'expliquerai pas ce concept dans mes cours, de très bon cours existent à ce sujet. Le cours OpenClassRooms Python par exemple.  
Si vraiment vous voulez en savoir plus, ou avoir plus d'explication [contactez moi](https://www.saschasalles.com/contact.php).  

## Utilisation

Pour utiliser le module tkinter la première chose à faire est de l'importer.  
Soit `from tkinter import *`   
De cette façon vous allez avoir accès à la totalité du module.  


### Notre première fenêtre !

Sans perdre de temps voyons tout de suite un premier exemple.
Notre exemple ci-dessous créer une fenêtre toute simple avec quelques widgets !
Et qu'est-ce qu'un widget ? Que signifie réelement ce mot ?
Il s'agit simplement de la contraction de `windows gadget`, il s'agit en soit d'un composant graphique.
Un bouton (button), un morceau de texte (label), une barre de défilement (ScrollBar) etc.., tous sont des widgets.

```python
from tkinter import * 
fen1 = Tk()
text1 = Label(fen1, text="Coucou les loulous !", fg="blue")
text1.pack()
boutton1 = Button(fen1, text='Quitter', command = fen1.destroy)
boutton1.pack()
fen1.mainloop()
```

**Analysons**   

  
* Sans surprise nous retrouvons l'importation du module tkinter à la première ligne.  
* À la deuxième ligne nous créons une d'instance de la classe TK() à savoir la fenêtre `fen1`.  
* Troisième ligne, nous créons une instance de la classe Label.
    * Remarquez qu'ici vous faites appel à une classe (Label) de la même façon que si vous appeliez une fonction, cette classe créer un objet Label sur la fenêtre          fen1 (instance de la classe Tk), contenant le texte "Coucou les loulous !", de couleur bleue.
* La ligne 4 -> Vous appelez la méthode de positionnement pack(), nous verrons plus tard une autre méthode de positionnement bcp plus précise.
* La ligne 5 -> Vous créez une instance de la classe Button, vous le positionnez sur la fen1, vous lui assignez le text "Quitter".
    * Concernant le paramètre `command` : 
        * Ce paramètre attend sois une fonction, sois une méthode que vous passez à un objet exemple fen1.destroy() qui tue la fenêtre.
* La ligne 6 -> Vous appelez la méthode de positionnement pack() pour placer cette fois ci le boutton1.
* La ligne 7 -> Vous appelez la méthode mainloop() de tkinter qui est un gestionnaire d'évenement, une sorte de boucle infinie qui attend qu'un truc ce passe.


**Structure d'un projet tkinter**   

Avec les GUIs les programmes ne sont plus destinés à être exécutés de la première à la dernière ligne mais à comporter tous les blocs d'instructions nécessaires pour répondre aux actions de l'utilisateur au clavier, à la souris, au joystick ...
Ainsi, dans un jeu vidéo, nous souhaitons, par exemple, que Mario Bros tourne à droite lorsque le joueur pousse son joystick vers la droite.

Nos programmes doivent désormais être pilotés par les événements qui se produisent (au clavier, à la souris, au joystick ...). Pour cela, nous devons céder la main à un réceptionnaire d'événements, capable de capter les actions de l'utilisateur et de les transmettre au programme pour les traiter.
C'est le rôle de l'instruction `fen.mainloop()` : il s'agit d'une boucle infinie qui va tourner jusqu'à la fermeture de la fenêtre, pour capter et transmettre toutes les actions de l'utilisateur. 

Une image va s'en doute mieux vous parler:
[structure](./screens/structuretkinter.png)



En conclusion ce n'est pas très compliqué de faire une interface graphique en tKinter. En revanche c'est moche et peu customisable.





